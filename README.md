ansible-cis-benchmark
=========

Ansible role to run the CIS benchmarks against hosts.

Requirements
------------

...

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Name | Default Value | Description
-----|---------------|------------
app_foo | bar | Sets app_foo to bar

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

Testing The Role
----------------

## Requirements

 * Docker
 * Vagrant

Testing uses Docker and Vagrant to run SSHD-enabled containers that allow Ansible to run this module.

1. Grab the `vagrant` SSH key pair from PasswordState (DevOps --> CI/CD Keys --> vagrant) and copy them to your `~/.ssh` folder as"
   ```
   ~/.ssh/vagrant
   ~/.ssh/vagrant.pub
   ```
1. Modify the tests as needed (located in the tests folder)
1. Test the role by using the script found in the root of this project, e.g.:
   ```
   ./run-test.sh centos7 up
   ```

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).