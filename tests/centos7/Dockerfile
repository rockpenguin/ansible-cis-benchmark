FROM centos:7
LABEL maintainer="PML"
ENV container=docker

# Install systemd -- See https://hub.docker.com/_/centos/
RUN yum -y update; yum clean all; \
(cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

# this prevents the ssh error 'System is booting up. See pam_nologin(8)'
RUN rm -f /usr/lib/tmpfiles.d/systemd-nologin.conf

# Install requirements.
RUN yum makecache fast && \
yum -y install deltarpm initscripts && \
yum -y update && \
yum -y install openssh-server openssh-clients curl less sudo && \
yum clean all

RUN /usr/bin/systemctl enable sshd.service

RUN useradd vagrant && \
usermod -a -G wheel vagrant && \
echo "%vagrant ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/vagrant

RUN cd /home/vagrant && \
mkdir .ssh && \
echo "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPoa/eEL2sJFoGaVa0OS6C+TQ9eSVhCQMVzkmQKpYx8jwkkksTafk3/T7lQyivraoToizJ6eQVRAjrgAGN1Jf6g= vagrant" > .ssh/authorized_keys && \
chown -R vagrant:vagrant .ssh && \
chmod 0600 .ssh/authorized_keys

EXPOSE 22

VOLUME ["/sys/fs/cgroup"]
CMD ["/usr/sbin/init"]
